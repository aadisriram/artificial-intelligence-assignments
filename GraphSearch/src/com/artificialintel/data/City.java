package com.artificialintel.data;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

public class City {

	private String cityName;
	private String expanded_from;
	private Map<City, Integer> routeTest;

	public String getExpanded_from() {
		return expanded_from;
	}

	public void setExpanded_from(String city) {
		this.expanded_from = city;
	}

	public City(String cityName) {
		this.cityName = cityName;
		routeTest = new TreeMap<City, Integer>(new ExampleComparator());
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public Map<City, Integer> getRouteTest() {
		return routeTest;
	}

	public void setRouteTest(Map<City, Integer> routeTest) {
		this.routeTest = routeTest;
	}

	public class ExampleComparator  implements Comparator<City>{
		public int compare(City obj1, City obj2) {
			return -(obj1.getCityName()).compareTo(obj2.getCityName());
		}
	}
}
