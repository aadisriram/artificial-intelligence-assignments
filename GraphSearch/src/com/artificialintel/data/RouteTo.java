package com.artificialintel.data;

public class RouteTo {

	private String destinationCity;
	private long distance;
	
	public RouteTo(String destinationCity, long distance) {
		this.destinationCity = destinationCity;
		this.distance = distance;
	}
	
	public String getDestinationCity() {
		return destinationCity;
	}
	
	public long getDistance() {
		return distance;
	}

	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	
	public void setDistance(long distance) {
		this.distance = distance;
	}
}
