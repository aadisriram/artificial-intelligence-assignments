package com.artificialintel.driver;

/*
 * Author     : Aaditya Sriram
 * Unity Id   : asriram4
 * Student Id : 200061280
 * 
 * Artificial Intelligence CSC520 : Assignment 2
 * This program finds paths using A*, Greedy and Uniform Cost Search for the given graph
 */


import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

public class AStarSearch {

	private static Map<String, Map<String, Integer>> routesMap;
	private static AStarSearch tf = new AStarSearch();
	private static Map<String, String> parentMap;
	private static Set<String> frontier;
	private static Set<String> explored;
	private static Map<String, Float> cost;
	private static Stack<String> finalPath;
	private static String sourceCityName;
	private static String destCityName;
	private static String searchType;
	private static Map<String, List<Float>> city_position;

	static String[] routes = new String[]{
		"albanyNY,montreal,226","albanyNY,boston,166","albanyNY,rochester,148",
		"albanyGA,tallahassee,120","albanyGA,macon,106",
		"albuquerque,elPaso,267","albuquerque,santaFe,61",
		"atlanta,macon,82","atlanta,chattanooga,117",
		"augusta,charlotte,161","augusta,savannah,131",
		"austin,houston,186","austin,sanAntonio,79",
		"bakersfield,losAngeles,112","bakersfield,fresno,107",
		"baltimore,philadelphia,102","baltimore,washington,45",
		"batonRouge,lafayette,50","batonRouge,newOrleans,80",
		"beaumont,houston,69","beaumont,lafayette,122",
		"boise,saltLakeCity,349","boise,portland,428",
		"boston,providence,51",
		"buffalo,toronto,105","buffalo,rochester,64","buffalo,cleveland,191",
		"calgary,vancouver,605","calgary,winnipeg,829",
		"charlotte,greensboro,91",
		"chattanooga,nashville,129",
		"chicago,milwaukee,90","chicago,midland,279",
		"cincinnati,indianapolis,110","cincinnati,dayton,56",
		"cleveland,pittsburgh,157","cleveland,columbus,142",
		"coloradoSprings,denver,70","coloradoSprings,santaFe,316",
		"columbus,dayton,72",
		"dallas,denver,792","dallas,mexia,83",
		"daytonaBeach,jacksonville,92","daytonaBeach,orlando,54",
		"denver,wichita,523","denver,grandJunction,246",
		"desMoines,omaha,135","desMoines,minneapolis,246",
		"elPaso,sanAntonio,580","elPaso,tucson,320",
		"eugene,salem,63","eugene,medford,165",
		"europe,philadelphia,3939",
		"ftWorth,oklahomaCity,209",
		"fresno,modesto,109",
		"grandJunction,provo,220",
		"greenBay,minneapolis,304","greenBay,milwaukee,117",
		"greensboro,raleigh,74",
		"houston,mexia,165",
		"indianapolis,stLouis,246",
		"jacksonville,savannah,140","jacksonville,lakeCity,113",
		"japan,pointReyes,5131","japan,sanLuisObispo,5451",
		"kansasCity,tulsa,249","kansasCity,stLouis,256","kansasCity,wichita,190",
		"keyWest,tampa,446",
		"lakeCity,tampa,169","lakeCity,tallahassee,104",
		"laredo,sanAntonio,154","laredo,mexico,741",
		"lasVegas,losAngeles,275","lasVegas,saltLakeCity,486",
		"lincoln,wichita,277","lincoln,omaha,58",
		"littleRock,memphis,137","littleRock,tulsa,276",
		"losAngeles,sanDiego,124","losAngeles,sanLuisObispo,182",
		"medford,redding,150",
		"memphis,nashville,210",
		"miami,westPalmBeach,67",
		"midland,toledo,82",
		"minneapolis,winnipeg,463",
		"modesto,stockton,29",
		"montreal,ottawa,132",
		"newHaven,providence,110","newHaven,stamford,92",
		"newOrleans,pensacola,268",
		"newYork,philadelphia,101",
		"norfolk,richmond,92","norfolk,raleigh,174",
		"oakland,sanFrancisco,8","oakland,sanJose,42",
		"oklahomaCity,tulsa,105",
		"orlando,westPalmBeach,168","orlando,tampa,84",
		"ottawa,toronto,269",
		"pensacola,tallahassee,120",
		"philadelphia,pittsburgh,319","philadelphia,newYork,101","philadelphia,uk1,3548",
		"philadelphia,uk2,3548",
		"phoenix,tucson,117","phoenix,yuma,178",
		"pointReyes,redding,215","pointReyes,sacramento,115",
		"portland,seattle,174","portland,salem,47",
		"reno,saltLakeCity,520","reno,sacramento,133",
		"richmond,washington,105",
		"sacramento,sanFrancisco,95","sacramento,stockton,51",
		"salinas,sanJose,31","salinas,sanLuisObispo,137",
		"sanDiego,yuma,172",
		"saultSteMarie,thunderBay,442","saultSteMarie,toronto,436",
		"seattle,vancouver,115",
		"thunderBay,winnipeg,440"
	};

	static String[] longLats = new String[] {
		"albanyGA,31.58,84.17",
		"albanyNY,42.66,73.78",
		"albuquerque,35.11,106.61",
		"atlanta,33.76,84.40",
		"augusta,33.43,82.02",
		"austin,30.30,97.75",
		"bakersfield,35.36,119.03",
		"baltimore,39.31,76.62",
		"batonRouge,30.46,91.14",
		"beaumont,30.08,94.13",
		"boise,43.61,116.24",
		"boston,42.32,71.09",
		"buffalo,42.90,78.85",
		"calgary,51.00,114.00",
		"charlotte,35.21,80.83",
		"chattanooga,35.05,85.27",
		"chicago,41.84,87.68",
		"cincinnati,39.14,84.50",
		"cleveland,41.48,81.67",
		"coloradoSprings,38.86,104.79",
		"columbus,39.99,82.99",
		"dallas,32.80,96.79",
		"dayton,39.76,84.20",
		"daytonaBeach,29.21,81.04",
		"denver,39.73,104.97",
		"desMoines,41.59,93.62",
		"elPaso,31.79,106.42",
		"eugene,44.06,123.11",
		"europe,48.87,-2.33",
		"ftWorth,32.74,97.33",
		"fresno,36.78,119.79",
		"grandJunction,39.08,108.56",
		"greenBay,44.51,88.02",
		"greensboro,36.08,79.82",
		"houston,29.76,95.38",
		"indianapolis,39.79,86.15",
		"jacksonville,30.32,81.66",
		"japan,35.68,220.23",
		"kansasCity,39.08,94.56",
		"keyWest,24.56,81.78",
		"lafayette,30.21,92.03",
		"lakeCity,30.19,82.64",
		"laredo,27.52,99.49",
		"lasVegas,36.19,115.22",
		"lincoln,40.81,96.68",
		"littleRock,34.74,92.33",
		"losAngeles,34.03,118.17",
		"macon,32.83,83.65",
		"medford,42.33,122.86",
		"memphis,35.12,89.97",
		"mexia,31.68,96.48",
		"mexico,19.40,99.12",
		"miami,25.79,80.22",
		"midland,43.62,84.23",
		"milwaukee,43.05,87.96",
		"minneapolis,44.96,93.27",
		"modesto,37.66,120.99",
		"montreal,45.50,73.67",
		"nashville,36.15,86.76",
		"newHaven,41.31,72.92",
		"newOrleans,29.97,90.06",
		"newYork,40.70,73.92",
		"norfolk,36.89,76.26",
		"oakland,37.80,122.23",
		"oklahomaCity,35.48,97.53",
		"omaha,41.26,96.01",
		"orlando,28.53,81.38",
		"ottawa,45.42,75.69",
		"pensacola,30.44,87.21",
		"philadelphia,40.72,76.12",
		"phoenix,33.53,112.08",
		"pittsburgh,40.40,79.84",
		"pointReyes,38.07,122.81",
		"portland,45.52,122.64",
		"providence,41.80,71.36",
		"provo,40.24,111.66",
		"raleigh,35.82,78.64",
		"redding,40.58,122.37",
		"reno,39.53,119.82",
		"richmond,37.54,77.46",
		"rochester,43.17,77.61",
		"sacramento,38.56,121.47",
		"salem,44.93,123.03",
		"salinas,36.68,121.64",
		"saltLakeCity,40.75,111.89",
		"sanAntonio,29.45,98.51",
		"sanDiego,32.78,117.15",
		"sanFrancisco,37.76,122.44",
		"sanJose,37.30,121.87",
		"sanLuisObispo,35.27,120.66",
		"santaFe,35.67,105.96",
		"saultSteMarie,46.49,84.35",
		"savannah,32.05,81.10",
		"seattle,47.63,122.33",
		"stLouis,38.63,90.24",
		"stamford,41.07,73.54",
		"stockton,37.98,121.30",
		"tallahassee,30.45,84.27",
		"tampa,27.97,82.46",
		"thunderBay,48.38,89.25",
		"toledo,41.67,83.58",
		"toronto,43.65,79.38",
		"tucson,32.21,110.92",
		"tulsa,36.13,95.94",
		"uk1,51.30,0.00",
		"uk2,51.30,0.00",
		"vancouver,49.25,123.10",
		"washington,38.91,77.01",
		"westPalmBeach,26.71,80.05",
		"wichita,37.69,97.34",
		"winnipeg,49.90,97.13",
		"yuma,32.69,114.62"
	};

	public static void main(String[] args) {

		sourceCityName = "losAngeles"; //args[1];
		destCityName = "sacramento"; //args[2];
		searchType = "greedy";//args[0];

		routesMap = new HashMap<String, Map<String, Integer>>();
		city_position = new HashMap<String, List<Float>>();
		
		for(String route : routes) {

			String[] splitRoute = route.split(",");
			if(!routesMap.containsKey(splitRoute[0].trim())) {
				Map<String, Integer> cityRoute = new TreeMap<String, Integer>();
				routesMap.put(splitRoute[0].trim(), cityRoute);
			}

			if(!routesMap.containsKey(splitRoute[1].trim())) {
				Map<String, Integer> cityRoute = new TreeMap<String, Integer>();
				cityRoute.put(splitRoute[0].trim(), Integer.parseInt(splitRoute[2].trim()));
				routesMap.put(splitRoute[1].trim(), cityRoute);
			}

			routesMap.get(splitRoute[0].trim()).put(splitRoute[1].trim(), Integer.parseInt(splitRoute[2].trim()));
			routesMap.get(splitRoute[1].trim()).put(splitRoute[0].trim(), Integer.parseInt(splitRoute[2].trim()));
		}

		for(String longLat : longLats) {
			String[] splitRoute = longLat.split(",");
			List<Float> position = new ArrayList<Float>();
			position.add(Float.valueOf(splitRoute[1]));
			position.add(Float.valueOf(splitRoute[2]));
			city_position.put(splitRoute[0].trim(), position);
		}

		cost = new HashMap<String, Float>();
		frontier = new TreeSet<String>(tf.new CityComparator());
		cost.put(sourceCityName, 0.0f);
		parentMap = new HashMap<String, String>();
		parentMap.put(sourceCityName, null);
		frontier.add(sourceCityName);
		explored = new HashSet<String>();
		
		switch(searchType) {
		case "astar"   :{
							frontier = new TreeSet<String>(tf.new HeuristicCityComparator());
							frontier.add(sourceCityName);
							doAStarSearch(); 
						}break;
		case "uniform" : doUniformCostSearch(); break;
		case "greedy"  : doGreedySearch(); break;
		}

		String next = destCityName;
		finalPath = new Stack<String>();
		finalPath.push(destCityName);
		while(next != null) {
			next = parentMap.get(next);
			if(next != null)
				finalPath.push(next);
		}
		System.out.println();
		
		int ct = 0;
		System.out.println("\n\n The final result path : ");
		while(!finalPath.isEmpty()) {
			ct++;
			System.out.print(" -> " + finalPath.pop());
		}
		
		System.out.println("\n\nTotal cost : " + cost.get(destCityName) + " and number of nodes : " + ct);
	}

	class CityComparator implements Comparator<String> {
		public int compare(String city1, String city2) {
			float cost_diff = cost.get(city1) - cost.get(city2);
			if(cost_diff == 0.0)
				return city1.compareTo(city2);

			return (int)cost_diff;
		}
	}
	
	class HeuristicCityComparator implements Comparator<String> {
		public int compare(String city1, String city2) {
			
			int cost_diff = (int)( ((cost.get(city1) + getHeuristic(city1, destCityName))
							- (cost.get(city2) + getHeuristic(city2, destCityName))) *100000);
			if(cost_diff == 0)
				return city1.compareTo(city2);

			return cost_diff;	
		}
	}

	private static void doUniformCostSearch() {
		boolean flag = false;
		int count = 0;
		System.out.println("The nodes expanded : ");
		while(!flag) {
			Iterator<String> it = frontier.iterator();
			String exploredCity = it.next();
			System.out.print(exploredCity + " -> ");
			count++;
			if(exploredCity.equals(destCityName))
				break;
			it.remove();
			explored.add(exploredCity);
			Map<String, Integer> curCity = routesMap.get(exploredCity);
			float curCost = cost.get(exploredCity);
			for(String city : curCity.keySet()) {
				if(!explored.contains(city)) {
					if(cost.containsKey(city) && frontier.contains(city)) {
						if(cost.get(city) >= curCost + curCity.get(city)) {
							frontier.remove(city);
							cost.put(city, curCost + curCity.get(city));
							parentMap.put(city, exploredCity);
							frontier.add(city);
						} else {
							frontier.add(city);
						}
					} else {
						cost.put(city, curCost + curCity.get(city));
						parentMap.put(city, exploredCity);
						frontier.add(city);
					}
				}
			}
		}
		
		System.out.println("\nCost : " + count);
	}

	private static void doAStarSearch() {
		boolean flag = false;
		int count = 0;
		System.out.println("The nodes expanded : ");
		while(!flag) {
			Iterator<String> it = frontier.iterator();
			String exploredCity = it.next();
			count++;
			System.out.print(exploredCity + " -> ");
			if(exploredCity.equals(destCityName))
				break;
			it.remove();
			explored.add(exploredCity);
			Map<String, Integer> curCity = routesMap.get(exploredCity);
			float curCost = cost.get(exploredCity);
			for(String city : curCity.keySet()) {
				if(!explored.contains(city)) {
					if(cost.containsKey(city) && frontier.contains(city)) {
						if(cost.get(city) >= curCost + curCity.get(city)) {
							frontier.remove(city);
							cost.put(city, curCost + curCity.get(city));
							parentMap.put(city, exploredCity);
							frontier.add(city);
						}
					} else {
						cost.put(city, curCost + curCity.get(city));
						parentMap.put(city, exploredCity);
						frontier.add(city);
					}
				}
			}
		}
		
		System.out.println("\n Count : " + count);
	}

	private static void doGreedySearch() {
		boolean flag = false;
		int count = 0;
		System.out.println("The nodes expanded : ");
		while(!flag) {
			Iterator<String> it = frontier.iterator();
			String exploredCity = it.next();
			System.out.print(exploredCity + " -> ");
			count++;
			if(exploredCity.equals(destCityName))
				break;
			it.remove();
			explored.add(exploredCity);
			Map<String, Integer> curCity = routesMap.get(exploredCity);
			for(String city : curCity.keySet()) {
				if(!explored.contains(city)) {
					cost.put(city, getHeuristic(city, destCityName));
					parentMap.put(city, exploredCity);
					frontier.add(city);
				}
			}
		}

		System.out.println("\n The number of nodes : " + count);
	}

	private static float getHeuristic(String source, String destination) {
		float xDiff = city_position.get(source).get(0) - city_position.get(destination).get(0);
		float xSum = city_position.get(source).get(0) + city_position.get(destination).get(0);
		float yDiff = city_position.get(source).get(1) - city_position.get(destination).get(1);
		double res = Math.sqrt(Math.pow(69.5*xDiff,2) + Math.pow(69.5 * (Math.cos(((xSum)/360) * Math.PI)) * (yDiff),2.0));
		return (float)res;
	}
}
