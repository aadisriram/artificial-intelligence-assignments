package com.artificialintel.driver;

/*
 * Author   : Aaditya Sriram
 * Unity Id : asriram4
 * 
 * Artificial Intelligence CSC520 : Assignment 1
 * This program finds paths using DFS and BFS for a given graph
 */


import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

public class SearchRomania {

	private static Map<String, Map<String, Integer>> routesMap;
	private static SearchRomania tf = new SearchRomania();
	private static Queue<String> searchQueue;
	private static String searchType;
	private static String sourceCityName;
	private static String destCityName;
	private static Stack<String> finalPath;
	private static Set<String> visited;
	private static Map<String, String> parentMap;
	private static Stack<String> searchStack;
	private static Stack<City> dfidSearchStack;
	private static SearchRomania sr = new SearchRomania();

	private class City {
		String name;
		int depth;
		String parent;
		
		public City(String name, int depth, String parent) {
			super();
			this.name = name;
			this.depth = depth;
			this.parent = parent;
		}
		
		public String getName() {
			return name;
		}
		
		public void setName(String name) {
			this.name = name;
		}
		public int getDepth() {
			return depth;
		}
		public void setDepth(int depth) {
			this.depth = depth;
		}
		public String getParent() {
			return parent;
		}
		public void setParent(String parent) {
			this.parent = parent;
		}
	}
	
	static String[] routes = new String[]{
		"oradea,sibiu,151",
		"oradea,zerind,71",
		"zerind,arad,75",
		"arad,sibiu,140",
		"arad,timisoara,118",
		"timisoara,lugoj,111",
		"lugoj,mehadia,70", 
		"dobreta,mehadia,75",
		"dobreta,craiova,120",
		"sibiu,rimnicu_vilcea,80",
		"sibiu,fagaras,99",
		"rimnicu_vilcea,craiova,146",
		"pitesti,craiova,138",
		"rimnicu_vilcea,pitesti,97",
		"bucharest,pitesti,101",
		"bucharest,fagaras,211",
		"bucharest,giurgiu,90",
		"bucharest,urziceni,85",
		"vaslui,urziceni,142",
		"hirsova,urziceni,98",
		"hirsova,eforie,86",
		"vaslui,iasi,92",
		"neamt,iasi,87"
	};
	
	public static void main(String[] args) {

		searchType = "bfs";
		sourceCityName = "pitesti";
		destCityName = "pitesti";
		
		dfidSearchStack = new Stack<City>();
		dfidSearchStack.push(sr.new City("pitesti", 1, null));
		parentMap = new HashMap<String, String>();

		StringComparator sc = tf.new StringComparator();
		searchQueue = new LinkedList<String>();

		routesMap = new HashMap<String, Map<String, Integer>>();
		for(String route : routes) {

			String[] splitRoute = route.split(",");
			if(!routesMap.containsKey(splitRoute[0])) {
				Map<String, Integer> cityRoute = new TreeMap<String, Integer>(sc);
				routesMap.put(splitRoute[0], cityRoute);
			}

			if(!routesMap.containsKey(splitRoute[1])) {
				Map<String, Integer> cityRoute = new TreeMap<String, Integer>(sc);
				cityRoute.put(splitRoute[0], Integer.parseInt(splitRoute[2]));
				routesMap.put(splitRoute[1], cityRoute);
			}

			routesMap.get(splitRoute[0]).put(splitRoute[1], Integer.parseInt(splitRoute[2]));
			routesMap.get(splitRoute[1]).put(splitRoute[0], Integer.parseInt(splitRoute[2]));
		}

		dfid(5);
		//findPath();
	}

	class StringComparator implements Comparator<String> {
		public int compare(String city1, String city2) {
			return -city1.compareTo(city2);
		}
	}

	private static boolean doBreadthFirstSearch() {
		visited = new HashSet<String>();
		parentMap = new HashMap<String, String>();
		searchQueue.add(sourceCityName);

		while(!searchQueue.isEmpty()) {
			String city = searchQueue.remove();
			if(city.equals(destCityName)) {
				return true;
			}

			for(String adjCity : routesMap.get(city).keySet()) {
				if(!visited.contains(adjCity)) {
					visited.add(adjCity);
					parentMap.put(adjCity, city);
					searchQueue.add(adjCity);
				}
			}
		}

		return true;
	}

	public static void findPath() {

		if(searchType.equals("dfs")) {
			searchStack = new Stack<String>();
			searchStack.push(sourceCityName);
			visited = new HashSet<String>();
			finalPath = new Stack<String>();
			if(doDepthFirstSearch()) {

				System.out.println(sourceCityName);
				while(!finalPath.isEmpty()) {
					System.out.println(finalPath.pop());
				}
			} else {
				System.out.println("No path found");
			} 
		}else if(searchType.equals("bfs")) {
			if(doBreadthFirstSearch()) {
				finalPath = new Stack<String>();
				String lastSeen = destCityName;
				while(lastSeen!= sourceCityName && routesMap.containsKey(sourceCityName)) {
					finalPath.add(parentMap.get(lastSeen));
					lastSeen = parentMap.remove(lastSeen);
				}

				while(!finalPath.isEmpty()) {
					System.out.println(finalPath.pop());
				}

				System.out.println(destCityName);
			} else {
				System.out.println("No path found");
			}
		}
	}

	private static boolean doDepthFirstSearch() {
		String currentCity = searchStack.peek();

		if(currentCity.equals(destCityName))
			return true;

		if(visited.contains(currentCity))
			return false;
		else
			visited.add(currentCity);

		boolean routeFound = false;
		for(String neighbor : routesMap.get(currentCity).keySet()) {
			if(visited.contains(neighbor))
				continue;
			searchStack.add(neighbor);
			routeFound = doDepthFirstSearch();
			if(routeFound) {
				finalPath.push(neighbor);
				searchStack.pop();
				return true;
			}
		}
		return false;
	}
	
	public static void dfid(int cutoff) {
		while(!dfidSearchStack.isEmpty()) {
			City city = dfidSearchStack.pop();
			System.out.print(city.getName() + " -> ");
			int depth = city.getDepth();
			if(depth < cutoff) {
				for(String neighbor : routesMap.get(city.getName()).keySet()) {
					if(!neighbor.equals(city.getParent())) {
						dfidSearchStack.add(sr.new City(neighbor, depth + 1, city.getName()));
					}
				}
			}
		}
	}
}
