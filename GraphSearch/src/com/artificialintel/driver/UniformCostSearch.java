package com.artificialintel.driver;

/*
 * Author     : Aaditya Sriram
 * Unity Id   : asriram4
 * Student Id : 200061280
 * 
 * Artificial Intelligence CSC520 : Assignment 2
 * This program finds paths using UniformCostSearch for the given graph
 */


import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

public class UniformCostSearch {

	private static Map<String, Map<String, Integer>> routesMap;
	private static UniformCostSearch tf = new UniformCostSearch();
	private static Map<String, String> parentMap;
	private static Set<String> frontier;
	private static Set<String> explored;
	private static Map<String, Integer> cost;
	private static Stack<String> finalPath;
	private static String sourceCityName;
	private static String destCityName;

	static String[] routes = new String[]{
		"albanyNY,montreal,226","albanyNY,boston,166","albanyNY,rochester,148",
		"albanyGA,tallahassee,120","albanyGA,macon,106",
		"albuquerque,elPaso,267","albuquerque,santaFe,61",
		"atlanta,macon,82","atlanta,chattanooga,117",
		"augusta,charlotte,161","augusta,savannah,131",
		"austin,houston,186","austin,sanAntonio,79",
		"bakersfield,losAngeles,112","bakersfield,fresno,107",
		"baltimore,philadelphia,102","baltimore,washington,45",
		"batonRouge,lafayette,50","batonRouge,newOrleans,80",
		"beaumont,houston,69","beaumont,lafayette,122",
		"boise,saltLakeCity,349","boise,portland,428",
		"boston,providence,51",
		"buffalo,toronto,105","buffalo,rochester,64","buffalo,cleveland,191",
		"calgary,vancouver,605","calgary,winnipeg,829",
		"charlotte,greensboro,91",
		"chattanooga,nashville,129",
		"chicago,milwaukee,90","chicago,midland,279",
		"cincinnati,indianapolis,110","cincinnati,dayton,56",
		"cleveland,pittsburgh,157","cleveland,columbus,142",
		"coloradoSprings,denver,70","coloradoSprings,santaFe,316",
		"columbus,dayton,72",
		"dallas,denver,792","dallas,mexia,83",
		"daytonaBeach,jacksonville,92","daytonaBeach,orlando,54",
		"denver,wichita,523","denver,grandJunction,246",
		"desMoines,omaha,135","desMoines,minneapolis,246",
		"elPaso,sanAntonio,580","elPaso,tucson,320",
		"eugene,salem,63","eugene,medford,165",
		"europe,philadelphia,3939",
		"ftWorth,oklahomaCity,209",
		"fresno,modesto,109",
		"grandJunction,provo,220",
		"greenBay,minneapolis,304","greenBay,milwaukee,117",
		"greensboro,raleigh,74",
		"houston,mexia,165",
		"indianapolis,stLouis,246",
		"jacksonville,savannah,140","jacksonville,lakeCity,113",
		"japan,pointReyes,5131","japan,sanLuisObispo,5451",
		"kansasCity,tulsa,249","kansasCity,stLouis,256","kansasCity,wichita,190",
		"keyWest,tampa,446",
		"lakeCity,tampa,169","lakeCity,tallahassee,104",
		"laredo,sanAntonio,154","laredo,mexico,741",
		"lasVegas,losAngeles,275","lasVegas,saltLakeCity,486",
		"lincoln,wichita,277","lincoln,omaha,58",
		"littleRock,memphis,137","littleRock,tulsa,276",
		"losAngeles,sanDiego,124","losAngeles,sanLuisObispo,182",
		"medford,redding,150",
		"memphis,nashville,210",
		"miami,westPalmBeach,67",
		"midland,toledo,82",
		"minneapolis,winnipeg,463",
		"modesto,stockton,29",
		"montreal,ottawa,132",
		"newHaven,providence,110","newHaven,stamford,92",
		"newOrleans,pensacola,268",
		"newYork,philadelphia,101",
		"norfolk,richmond,92","norfolk,raleigh,174",
		"oakland,sanFrancisco,8","oakland,sanJose,42",
		"oklahomaCity,tulsa,105",
		"orlando,westPalmBeach,168","orlando,tampa,84",
		"ottawa,toronto,269",
		"pensacola,tallahassee,120",
		"philadelphia,pittsburgh,319","philadelphia,newYork,101","philadelphia,uk1,3548",
		"philadelphia,uk2,3548",
		"phoenix,tucson,117","phoenix,yuma,178",
		"pointReyes,redding,215","pointReyes,sacramento,115",
		"portland,seattle,174","portland,salem,47",
		"reno,saltLakeCity,520","reno,sacramento,133",
		"richmond,washington,105",
		"sacramento,sanFrancisco,95","sacramento,stockton,51",
		"salinas,sanJose,31","salinas,sanLuisObispo,137",
		"sanDiego,yuma,172",
		"saultSteMarie,thunderBay,442","saultSteMarie,toronto,436",
		"seattle,vancouver,115",
		"thunderBay,winnipeg,440"
	};

	static String[] positions = new String[] {

	};

	public static void main(String[] args) {

		sourceCityName = "portland";
		destCityName = "sacramento";

		routesMap = new HashMap<String, Map<String, Integer>>();
		for(String route : routes) {

			String[] splitRoute = route.split(",");
			if(!routesMap.containsKey(splitRoute[0])) {
				Map<String, Integer> cityRoute = new TreeMap<String, Integer>();
				routesMap.put(splitRoute[0], cityRoute);
			}

			if(!routesMap.containsKey(splitRoute[1])) {
				Map<String, Integer> cityRoute = new TreeMap<String, Integer>();
				cityRoute.put(splitRoute[0], Integer.parseInt(splitRoute[2]));
				routesMap.put(splitRoute[1], cityRoute);
			}

			routesMap.get(splitRoute[0]).put(splitRoute[1], Integer.parseInt(splitRoute[2]));
			routesMap.get(splitRoute[1]).put(splitRoute[0], Integer.parseInt(splitRoute[2]));
		}

		cost = new HashMap<String, Integer>();
		frontier = new TreeSet<String>(tf.new CityComparator());
		cost.put(sourceCityName, 0);
		parentMap = new HashMap<String, String>();
		parentMap.put(sourceCityName, null);
		frontier.add(sourceCityName);
		explored = new HashSet<String>();
		doUniformCostSearch();

		String next = destCityName;
		finalPath = new Stack<String>();
		finalPath.push(destCityName);
		while(next != null) {
			next = parentMap.get(next);
			if(next != null)
				finalPath.push(next);
		}

		while(!finalPath.isEmpty()) {
			System.out.println(finalPath.pop());
		}
	}

	class CityComparator implements Comparator<String> {
		public int compare(String city1, String city2) {
			int cost_diff = cost.get(city1) - cost.get(city2);
			if(cost_diff == 0)
				return city1.compareTo(city2);
			
			return cost_diff;
		}
	}

	private static void doUniformCostSearch() {
		boolean flag = false;
		while(!flag) {
			Iterator<String> it = frontier.iterator();
			String exploredCity = it.next();
			if(exploredCity.equals(destCityName))
				break;
			it.remove();
			explored.add(exploredCity);
			Map<String, Integer> curCity = routesMap.get(exploredCity);
			int curCost = cost.get(exploredCity);
			for(String city : curCity.keySet()) {
				if(!explored.contains(city)) {
					if(cost.containsKey(city) && frontier.contains(city)) {
						if(cost.get(city) >= curCost + curCity.get(city)) {
							frontier.remove(city);
							cost.put(city, curCost + curCity.get(city));
							parentMap.put(city, exploredCity);
							frontier.add(city);
						} else {
							frontier.add(city);
						}
					} else {
						cost.put(city, curCost + curCity.get(city));
						parentMap.put(city, exploredCity);
						frontier.add(city);
					}
				}
			}
		}
	}
}