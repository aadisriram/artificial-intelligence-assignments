package com.artficialintel.search;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

import com.artificialintel.data.City;

public class BreadthFirstSearch implements Search {

	private Queue<City> searchQueue;
	private Set<City> visited;
	private City source;
	private City destination;
	private Stack<City> finalPath;
	Map<City, City> parentMap;

	public BreadthFirstSearch(City source, City destination) {
		this.source = source;
		this.destination = destination;
		searchQueue = new LinkedList<City>();
		finalPath = new Stack<City>();
		searchQueue.add(source);
		visited = new HashSet<City>();
		parentMap = new HashMap<City, City>();
	}

	public Queue<City> getSearchStack() {
		return searchQueue;
	}

	public void setSearchStack(Queue<City> searchStack) {
		this.searchQueue = searchStack;
	}

	public City getSource() {
		return source;
	}

	public void setSource(City source) {
		this.source = source;
	}

	public City getDestination() {
		return destination;
	}

	public void setDestination(City destination) {
		this.destination = destination;
	}

	public Set<City> getVisited() {
		return visited;
	}

	public void setVisited(Set<City> visited) {
		this.visited = visited;
	}

	@Override
	public void findPath() {
		if(doBreadthFirstSearch()) {
			
			System.out.println("\nThe path available using BFS :");
			City lastSeen = destination;
			while(lastSeen!= source && parentMap.containsKey(source)) {
				finalPath.add(parentMap.get(lastSeen));
				lastSeen = parentMap.remove(lastSeen);
			}
			
			while(!finalPath.isEmpty()) {
				System.out.println(finalPath.pop().getCityName());
			}
			
			System.out.println(destination.getCityName());
		} else {
			System.out.println("No path found");
		}
	}

	private boolean doBreadthFirstSearch() {
		while(!searchQueue.isEmpty()) {
			City city = searchQueue.remove();
			if(city.equals(destination)) {
				return true;
			}
			
			for(City adjCity : city.getRouteTest().keySet()) {
				if(!visited.contains(adjCity)) {
					visited.add(adjCity);
					parentMap.put(adjCity, city);
					searchQueue.add(adjCity);
				}
			}
		}
		
		return true;
	}
}
