package com.artficialintel.search;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import com.artificialintel.data.City;

public class DepthFirstSearch implements Search {

	private Stack<City> searchStack;
	private Set<City> visited;
	private City source;
	private City destination;
	private Stack<City> finalPath;

	public DepthFirstSearch(City source, City destination) {
		this.source = source;
		this.destination = destination;
		searchStack = new Stack<City>();
		finalPath = new Stack<City>();
		searchStack.push(source);
		visited = new HashSet<City>();
	}

	public Stack<City> getSearchStack() {
		return searchStack;
	}

	public void setSearchStack(Stack<City> searchStack) {
		this.searchStack = searchStack;
	}

	public City getSource() {
		return source;
	}

	public void setSource(City source) {
		this.source = source;
	}

	public City getDestination() {
		return destination;
	}

	public void setDestination(City destination) {
		this.destination = destination;
	}

	public Set<City> getVisited() {
		return visited;
	}

	public void setVisited(Set<City> visited) {
		this.visited = visited;
	}

	@Override
	public void findPath() {
		if(doDepthFirstSearch()) {
			
			System.out.println("The path available is : \n"  + source.getCityName());
			while(!finalPath.isEmpty()) {
				System.out.println(finalPath.pop().getCityName());
			}
		} else {
			System.out.println("No path found");
		}
	}

	private boolean doDepthFirstSearch() {
		City currentCity = searchStack.peek();

		if(currentCity.getCityName() == destination.getCityName())
			return true;

		if(visited.contains(currentCity))
			return false;
		else
			visited.add(currentCity);

		boolean routeFound = false;
		for(City neighbor : currentCity.getRouteTest().keySet()) {
			if(visited.contains(neighbor))
				continue;
			searchStack.add(neighbor);
			routeFound = doDepthFirstSearch();
			if(routeFound) {
				finalPath.push(neighbor);
				searchStack.pop();
				return true;
			}
		}

		return false;
	}
}
