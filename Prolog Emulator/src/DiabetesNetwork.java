import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;


public class DiabetesNetwork {

	static Map<Node, Map<Slot, Node>> edgeMap = new HashMap<Node, Map<Slot, Node>>();

	static enum Slot {
		isa, shouldAvoid, contains, ako
	}

	static enum Node {
		david, diabetics, sugar, candy, snickers
	}

	static int memoize[][][] = new int[Node.values().length][Slot.values().length][Node.values().length];

	public static boolean findEdge(Node node, Slot slot, Node value) {
		if(edgeMap.containsKey(node)) {
			Map<Slot, Node> map = edgeMap.get(node);
			if(map.containsKey(slot)) {
				if(map.get(slot).equals(value))
					return true;
			}
		}
		return false;
	}

	static boolean findValue(Node node, Slot slot, Node value) {

		int val = memoize[node.ordinal()][slot.ordinal()][value.ordinal()];
		if(val > 0) {
			if(val == 1)
				return false;
			else
				return true;
		}

		if(findEdge(node, slot, value)) {
			memoize[node.ordinal()][slot.ordinal()][value.ordinal()] = 2;
			return true;
		} else {
			memoize[node.ordinal()][slot.ordinal()][value.ordinal()] = 1;
		}

		boolean getValue = false;
		for(Node n : Node.values()) {

			if(n.equals(node))
				continue;

			//			if(edgeMap.containsKey(n))
			//				if(edgeMap.get(n).containsKey(slot))
			//					if(edgeMap.get(n).get(slot).equals(node))
			//						continue;

			boolean edge = findEdge(node, Slot.isa, n);

			if(edge) {
				val = memoize[n.ordinal()][slot.ordinal()][value.ordinal()];
				if(val > 0) {
					if(val == 1)
						return false;
					else
						return true;
				}
				getValue = findValue(n, slot, value);
				memoize[node.ordinal()][slot.ordinal()][value.ordinal()] = (getValue == true)? 2: 1;
				if(getValue) {
					return true;
				}
			} else {
				edge = findEdge(node, Slot.ako, n);
				if(edge) {
					val = memoize[n.ordinal()][slot.ordinal()][value.ordinal()];
					if(val > 0) {
						if(val == 1)
							return false;
						else
							return true;
					}
					getValue = findValue(n, slot, value);
					memoize[node.ordinal()][slot.ordinal()][value.ordinal()] = (getValue == true)? 2: 1;
					if(getValue) {
						return true;
					}
				} else {
					if(slot.equals(Slot.shouldAvoid)) {
						if(!(n.equals(value) || n.equals(node) || 
								n.equals(Node.snickers) || n.equals(Node.david))) {

							boolean val1 = false, val2 = false;
							val = memoize[value.ordinal()][Slot.contains.ordinal()][n.ordinal()];
							if(val > 0) {
								if(val == 2)
									val1 = true;
							} else {
								val1 = findValue(value, Slot.contains, n);
								memoize[value.ordinal()][Slot.contains.ordinal()][n.ordinal()] = val1 ? 2:1;
							}

							val = memoize[node.ordinal()][Slot.shouldAvoid.ordinal()][n.ordinal()];
							if(val > 0) {
								if(val == 2)
									val2 = true;
							} else {
								val2 = findValue(node, Slot.shouldAvoid, n);
								memoize[node.ordinal()][Slot.shouldAvoid.ordinal()][n.ordinal()] = val2 ? 2:1;
							}

							if(val1 && val2) {
								memoize[node.ordinal()][slot.ordinal()][value.ordinal()] = 2;
								return true;
							} else {
								memoize[node.ordinal()][slot.ordinal()][value.ordinal()] = 1;
							}
						}
					}
				}
			}
		}

		return false;
	}	

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		Map<Slot, Node> tempMap = new HashMap<Slot, Node>();

		tempMap.put(Slot.isa, Node.diabetics);
		edgeMap.put(Node.david, new HashMap<Slot, Node>(tempMap));
		tempMap.clear();

		tempMap.put(Slot.shouldAvoid, Node.sugar);
		edgeMap.put(Node.diabetics, new HashMap<Slot, Node>(tempMap));
		tempMap.clear();

		tempMap.put(Slot.contains, Node.sugar);
		edgeMap.put(Node.candy, new HashMap<Slot, Node>(tempMap));
		tempMap.clear();

		tempMap.put(Slot.ako, Node.candy);
		edgeMap.put(Node.snickers, new HashMap<Slot, Node>(tempMap));
		tempMap.clear();

		Scanner s = new Scanner(System.in);
		String input = "";

		while(true) {

			input = s.nextLine();
			if(input.equals("exit."))
				break;
			
			if(input.length() == 0) {
				continue;
			}
			
			String[] split = input.split("\\(");
			String opType = split[0];
			split = split[1].split("\\)");
			split = split[0].split(",");

			Set<Node> nodes = new LinkedHashSet<Node>();
			Set<Node> values = new LinkedHashSet<Node>();
			Set<Slot> slots = new LinkedHashSet<Slot>();

			Node node, value;
			Slot slot;
			boolean nodeVal = true, slotVal = true, valueVal = true;
			String nodeStr = "", slotStr = "", valStr = "";

			try {
				node = Node.valueOf(split[0].trim());
				nodes.add(node);
			} catch(Exception e) {
				nodeVal = false;
				nodeStr = split[0].trim();
				nodes.addAll(Arrays.asList(Node.values()));
			}

			try {
				slot = Slot.valueOf(split[1].trim());
				slots.add(slot);
			} catch(Exception e) {
				slotVal = false;
				slotStr = split[1].trim();
				slots.addAll(Arrays.asList(Slot.values()));
			}

			try {
				value = Node.valueOf((split[2].split("\\)")[0]).trim());
				values.add(value);
			} catch(Exception e) {
				valueVal = false;
				valStr = (split[2].split("\\)")[0]).trim();
				values.addAll(Arrays.asList(Node.values()));
			}

//			values.remove(Node.david);
//			nodes.remove(Node.sugar);
			boolean sameNodes = false;
			if(nodeStr.length() > 1){
				if(nodeStr.equals(valStr)) {
					nodes = values;
					sameNodes = true;
				}
				
				if(nodeStr.equals(slotStr) || (valStr.equals(slotStr) && valStr.length() > 1)) {
					System.out.println(false);
					input = s.nextLine();
					if(input.equals("exit."))
						break;
					
					continue;
				}
			}

			boolean n = false;
			for(Node n1 : nodes) {
				for(Slot s1 : slots) {
					for(Node n2 : values) {
						if(n2.equals(n1))
							continue;

						if(edgeMap.containsKey(n2))
							if(edgeMap.get(n2).containsKey(s1))
								if(edgeMap.get(n2).get(s1).equals(n1))
									continue;

						if(opType.equals("edge")) {
							n = findEdge(n1, s1, n2);
						} else if(opType.equals("value")) {
							n = findValue(n1, s1, n2);
						}

						if(n) {
							if(!nodeVal || !slotVal || !valueVal) {
								
								if(sameNodes) {
									if(!n1.equals(n2))
										continue;
								}
								
								if(!nodeVal)
									System.out.println(nodeStr + " = " + n1);

								if(!slotVal)
									System.out.println(slotStr + " = " + s1);

								if(!valueVal)
									System.out.println(valStr + " = " + n2);
								
								s.nextLine();
							} else {
								System.out.println(true);
							}
						}
					}
				}
			}

			if(!n) {
				System.out.println(false);
			}
		}
	}
}
